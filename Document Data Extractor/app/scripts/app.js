(function(document) {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'login': '/',
      'extract-landing': '/extract-landing',
      'extract-listing': '/extract-listing',
      'train': '/train',
      'train-landing': '/train-landing'
    }
  });
}(document));
